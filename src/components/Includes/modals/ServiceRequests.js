import React, {useState, useEffect} from 'react';
import {
  SafeAreaView,
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
  Dimensions,
  TextInput,
} from 'react-native';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import DateTimePickerModal from 'react-native-modal-datetime-picker';

Icon.loadFont();
const {width, height} = Dimensions.get('window');

export default function ServiceRequests(props) {
  const [isDateVisible, setDateVisible] = useState(false);
  const [date, setDate] = useState('dd/mm/yyyy');
  let hideDatePicker = () => {
    setDateVisible(false);
  };

  let handleConfirm = (date) => {
    console.warn(date);
    let text_date = JSON.stringify(date);
    // console.warn(typeof text_date);
    setDateVisible(false);
    setDate(text_date.substring(1, 11));
  };

  return (
    <View style={styles.mainContainer}>
      <View style={styles.ContentBox}>
        <View style={styles.headerContainer}>
          <TouchableOpacity onPress={() => props.setVisible(false)}>
            <Icon name="arrow-left" size={30} color="#000" />
          </TouchableOpacity>
          <View style={styles.heading}>
            <Text style={styles.headingText}> New service Request</Text>
          </View>
        </View>
        <View style={styles.topContainer}>
          <View style={styles.main}>
            <View style={styles.Details}>
              <TextInput
                style={styles.input}
                placeholder="Issue"
                //   onChangeText={(val) => setPassword(val)}
                placeholderStyle={styles.textboxfieldd}
              />
            </View>
            <View style={styles.Details}>
              <TextInput
                style={styles.input}
                placeholder="Issue Details"
                //   onChangeText={(val) => setPassword(val)}
                placeholderStyle={styles.textboxfieldd}
              />
            </View>
            <View
              style={[
                styles.Details,
                {
                  flexDirection: 'row',
                  justifyContent: 'space-between',
                  alignItems: 'center',
                },
              ]}>
              <TextInput
                style={styles.input}
                placeholder="Date"
                editable={false}
                value={date}
                //   onChangeText={(val) => setPassword(val)}
                placeholderStyle={styles.textboxfieldd}
              />
              <DateTimePickerModal
                isVisible={isDateVisible}
                mode="date"
                onConfirm={(date) => {
                  console.warn(date);
                  handleConfirm(date);
                }}
                onCancel={hideDatePicker}
              />
              <Icon
                name="calendar-month"
                size={20}
                color="blue"
                onPress={() => {
                  setDateVisible(true);
                }}
              />
            </View>
            <View
              style={[
                styles.Details,
                {
                  flexDirection: 'row',
                  justifyContent: 'space-between',
                  // alignContent: 'center',
                },
              ]}>
              <TextInput
                style={styles.input}
                placeholder="Priority"
                //   onChangeText={(val) => setPassword(val)}
                placeholderStyle={styles.textboxfieldd}
              />
              {/* <Icon name="chevron-down" size={26} color="#3F51B5" /> */}
            </View>
          </View>
          <TouchableOpacity
            onPress={() => {
              props.setUpload(true);
              props.setVisible(false);
            }}
            style={styles.bottomContainer}>
            <Text style={styles.buttonText}>Next</Text>
          </TouchableOpacity>
        </View>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  mainContainer: {
    position: 'absolute',
    bottom: 0,
    left: 0,
    // height: height - 20,
    width: width,
    // paddingVertical: 29,
    // paddingHorizontal: 20,
    justifyContent: 'flex-end',
    backgroundColor: 'rgba(0,0,0,0.5)',
  },
  topContainer: {
    // justifyContent: "center",
  },
  headerContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    // paddingVertical: 20,
    marginBottom: 10,
  },
  headingText: {
    fontSize: 20,
    fontFamily: 'ProductSans-Bold',
  },
  heading: {
    flex: 0.82,
  },
  main: {
    backgroundColor: '#fff',
    marginBottom: 30,
  },
  Details: {
    borderBottomWidth: 1,
    borderColor: '#ccc',
  },

  ContentBox: {
    backgroundColor: '#fff',
    padding: 20,
  },
  bottomContainer: {
    alignItems: 'center',
    width: '100%',

    padding: 10,
    borderRadius: 10,
    backgroundColor: '#3F51B5',
  },

  button: {
    width: '100%',
    paddingVertical: 13,
  },
  buttonText: {
    color: '#fff',
    textAlign: 'center',
    fontFamily: 'ProductSans-Regular',
  },
});
