import React, {useState} from 'react';
import {
  SafeAreaView,
  StyleSheet,
  Text,
  View,
  ScrollView,
  TouchableOpacity,
  Dimensions,
  Image,
} from 'react-native';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import PhotoUpload from 'react-native-photo-upload';

Icon.loadFont();
const {width, height} = Dimensions.get('window');

const ScanCodeCard = (props) => {
  const [image, setImage] = useState(null);
  const [setAvathar, setsetAvathar] = useState(null);
  const {navigation} = props;

  const showImage = () => {
    if (image == null) {
      return (
        <Image
          source={require('../../../assets/images/vectors/upload.png')}
          style={styles.image}
        />
      );
    } else {
      return <Image source={image} style={styles.image} />;
    }
  };

  return (
    <View style={styles.mainContainer}>
      <View style={styles.ContentBox}>
        <View style={styles.headerContainer}>
          <TouchableOpacity
            onPress={() => {
              props.setUpload(false);
              props.setVisible(true);
            }}>
            <Icon name="arrow-left" size={30} color="#3F51B5" />
          </TouchableOpacity>
          <View style={styles.heading}>
            <Text style={styles.headingText}>Upload Image</Text>
          </View>
        </View>
        <View style={styles.topContainer}>
          <PhotoUpload
            onPhotoSelect={(avatar) => {
              if (avatar) {
                const image_uri = {uri: 'data:image/jpeg;base64,' + avatar};
                setImage(avatar);
                setAvathar(avatar);
              }
            }}
            style={styles.MainImageCard}>
            <View style={styles.main}>
              <View style={styles.ImageContainer}>{showImage()}</View>
              <Text style={{fontSize: 11, textAlign: 'center'}}>
                Uploading more photos
              </Text>
            </View>
          </PhotoUpload>
          <View style={styles.IconContainer}>
            <View style={styles.IconBox}>
              <View>
                <Icon name="camera" style={styles.cameraIcon} />
              </View>
              <View>
                <Text style={styles.bottomContainerText}>Camera</Text>
              </View>
            </View>
            <View style={styles.IconBox}>
              <Icon name="folder" size={30} color="#3F51B5" />
              <Text style={styles.bottomContainerText}>Open Folder</Text>
            </View>
          </View>
          <TouchableOpacity
            style={styles.bottomContainer}
            onPress={() => {
              props.setUpload(false);
              props.setVisible(false);
              props.navigation.navigate('Success');
            }}>
            <Text style={styles.buttonText}>Submit</Text>
          </TouchableOpacity>
        </View>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  mainContainer: {
    width: width,
    minHeight: '100%',
    justifyContent: 'flex-end',
    backgroundColor: 'rgba(0,0,0,0.5)',
  },
  topContainer: {
    alignItems: 'center',
    height: height * 0.35,
  },
  headerContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    paddingVertical: 20,
    marginBottom: 20,
  },
  headingText: {
    fontSize: 20,
    fontFamily: 'ProductSans-Bold',
  },
  heading: {
    flex: 0.67,
  },
  ImageContainer: {
    width: 120,
    height: 85,
    padding: 2,
  },
  image: {
    width: '100%',
    height: '100%',
  },
  main: {
    backgroundColor: '#fff',
    justifyContent: 'center',
    marginBottom: 30,
    borderColor: 'red',
  },
  IconContainer: {
    justifyContent: 'space-between',
    flexDirection: 'row',
    width: width - 40,
    alignItems: 'center',
    marginBottom: 20,
  },
  IconBox: {
    flexDirection: 'row',
    width: width * 0.4,
    borderWidth: 1,
    borderColor: '#3F51B5',
    borderRadius: 10,
    paddingVertical: 10,
    paddingHorizontal: '4%',
    alignItems: 'center',
  },
  ContentBox: {
    backgroundColor: '#fff',
    padding: 20,
    borderTopLeftRadius: 30,
    borderTopRightRadius: 30,
  },
  bottomContainer: {
    alignItems: 'center',
    width: width - 20,
    elevation: 6,
    backgroundColor: '#3F51B5',
    paddingVertical: 13,
    borderRadius: 10,
  },
  bottomContainerText: {
    fontFamily: 'ProductSans-Regular',
    fontSize: 10,
    marginLeft: 5,
  },
  cameraIcon: {
    fontSize: 30,
    color: '#3F51B5',
  },
  button: {
    width: '100%',
    paddingVertical: 13,
  },
  buttonText: {
    color: '#fff',
    textAlign: 'center',
    fontFamily: 'ProductSans-Regular',
  },
});

export default ScanCodeCard;
