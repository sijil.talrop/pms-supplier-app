import React, {useState} from 'react';
import {
  SafeAreaView,
  ScrollView,
  View,
  ImageBackground,
  Image,
  StyleSheet,
  Text,
  Button,
  Modal,
  Dimensions,
} from 'react-native';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import {TouchableOpacity} from 'react-native-gesture-handler';

export default function ServiceRequest(props) {
  return (
    <ScrollView>
      <View style={styles.top}>
        <TouchableOpacity onPress={() => props.setVisible(true)}>
          <Icon name="arrow-left" size={25} onPress={props.onPress} />
        </TouchableOpacity>
        <Text style={styles.header}>Order Summary</Text>
      </View>
      <View style={styles.address}>
        <Text style={styles.name}>Martin</Text>
        <Text style={styles.addressDetail}>
          Kalathil house,edavanna,Malappuram District,Kerala Pincode - 676541
        </Text>
        <Text style={styles.number}>91 9568 9856</Text>
      </View>
      <View style={styles.OrderItems}>
        <View style={styles.cartCard}>
          <TouchableOpacity style={styles.cartLeft}>
            <Image
              style={styles.itemImage}
              source={require('../../assets/images/meat.jpg')}
            />
          </TouchableOpacity>

          <View style={styles.cartMiddle}>
            <TouchableOpacity>
              <Text style={styles.title}>Liquolo Odio nisl</Text>
            </TouchableOpacity>
            <Text style={styles.subtitle}>Integer at faucibolo urna.</Text>
            <Text style={styles.subtitle}>Nullam condimentam leo</Text>
            <View style={{flexDirection: 'row', alignItems: 'center'}}>
              <Text style={{color: '#000'}}>4.2</Text>
              <Icon name="star" />
            </View>
          </View>
          <View style={styles.cartRight}>
            <Text style={styles.offerPrice}>₹668</Text>
            <Text style={styles.price}>₹788</Text>
            <Text style={styles.discount}>60% off</Text>
            <TouchableOpacity>
              <Icon name="delete" style={styles.deleteIcon} />
            </TouchableOpacity>
          </View>
        </View>
        <View style={styles.cartCard}>
          <TouchableOpacity style={styles.cartLeft}>
            <Image
              style={styles.itemImage}
              source={require('../../assets/images/meat.jpg')}
            />
          </TouchableOpacity>

          <View style={styles.cartMiddle}>
            <TouchableOpacity>
              <Text style={styles.title}>Liquolo Odio nisl</Text>
            </TouchableOpacity>
            <Text style={styles.subtitle}>Integer at faucibolo urna.</Text>
            <Text style={styles.subtitle}>Nullam condimentam leo</Text>
            <View style={{flexDirection: 'row', alignItems: 'center'}}>
              <Text style={{color: '#000'}}>4.2</Text>
              <Icon name="star" />
            </View>
          </View>
          <View style={styles.cartRight}>
            <Text style={styles.offerPrice}>₹668</Text>
            <Text style={styles.price}>₹788</Text>
            <Text style={styles.discount}>60% off</Text>
            <TouchableOpacity>
              <Icon name="delete" style={styles.deleteIcon} />
            </TouchableOpacity>
          </View>
        </View>
        <View style={styles.estimated}>
          <Text style={{fontWeight: 'bold', fontSize: 13}}>
            Estimated delivery time
          </Text>
          <Text style={{fontWeight: 'bold', color: '#000'}}>30 min</Text>
        </View>
        <View style={styles.priceBox}>
          <Text style={styles.priceHeader}>Price Details</Text>
          <View style={styles.fieldBox}>
            <View style={styles.field}>
              <Text style={{fontSize: 12}}>Price(2 items)</Text>
              <Text style={{fontSize: 12}}>₹978</Text>
            </View>
            <View style={styles.field}>
              <Text style={{fontSize: 12}}>Delivery</Text>
              <Text style={{fontSize: 12}}>Free</Text>
            </View>
          </View>
          <View style={styles.totalField}>
            <Text style={{color: '#000'}}>Amount Payable</Text>
            <Text style={{color: '#000'}}>₹978</Text>
          </View>
        </View>
        <View style={styles.estimated}>
          <Text style={{fontWeight: 'bold', fontSize: 13}}>Payment method</Text>
          <Text style={{fontWeight: 'bold', color: '#B31220'}}>
            Cash on delivery
          </Text>
        </View>
        <View style={styles.orderButtons}>
          <View style={styles.cancelButton}>
            <TouchableOpacity>
              <Text>Cancel</Text>
            </TouchableOpacity>
          </View>
          <View style={styles.buyButton}>
            <TouchableOpacity>
              <Text style={{color: '#fff'}} onPress={props.onPress}>
                Buy Now
              </Text>
            </TouchableOpacity>
          </View>
        </View>
      </View>
    </ScrollView>
  );
}
const styles = StyleSheet.create({
  top: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    width: '80%',
    marginBottom: 20,
  },
  header: {
    fontSize: 25,
    color: '#000',
    fontWeight: 'bold',
  },
  address: {
    borderBottomWidth: 2,
    borderColor: '#f9f9f9',
    paddingVertical: 10,
  },
  name: {
    fontSize: 18,
    color: '#5F5F5F',
  },
  addressDetail: {
    marginVertical: 5,
    color: '#5F5F5F',
  },
  number: {
    color: '#5F5F5F',
  },
  bottom: {},
  OrderItems: {
    paddingVertical: 20,
  },
  cartCard: {
    width: width,
    flexDirection: 'row',
    alignItems: 'center',
    marginBottom: 20,
  },
  cartLeft: {
    overflow: 'hidden',
    resizeMode: 'contain',
    width: width * 0.25,
    marginRight: 10,
  },
  itemImage: {
    width: width * 0.25,
    height: 100,
  },
  cartMiddle: {
    width: width * 0.4,
    paddingVertical: 2,
    alignItems: 'flex-start',
  },
  title: {
    color: '#000',
    fontWeight: 'bold',
  },
  subtitle: {
    fontSize: 12,
  },
  cartRight: {
    width: width * 0.25,
    padding: 2,
    paddingLeft: 40,
  },
  offerPrice: {
    color: '#4C4C4C',
  },
  deleteIcon: {
    color: '#BE0517',
    fontSize: 20,
    marginTop: 4,
  },
  price: {
    fontSize: 10,
    textDecorationLine: 'line-through',
  },
  discount: {
    color: '#4BB358',
    fontWeight: 'bold',
    fontSize: 10,
  },
  estimated: {
    paddingHorizontal: 10,
    paddingVertical: 5,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    borderWidth: 1,
    borderColor: '#F3F3F3',
    marginBottom: 20,
  },
  priceBox: {
    borderWidth: 1,
    borderColor: '#F3F3F3',
    padding: 10,
    marginBottom: 20,
  },
  priceHeader: {
    fontSize: 14,
    paddingVertical: 10,
    borderBottomWidth: 1,
    borderBottomColor: '#F3F3F3',
  },
  fieldBox: {
    paddingVertical: 10,
    borderBottomWidth: 1,
    borderBottomColor: '#F3F3F3',
  },
  field: {
    flexDirection: 'row',
    marginBottom: 5,
    justifyContent: 'space-between',
  },
  totalField: {
    flexDirection: 'row',
    paddingVertical: 10,
    justifyContent: 'space-between',
  },
  orderButtons: {
    paddingVertical: 20,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  cancelButton: {
    padding: 10,
    borderWidth: 1,
    borderColor: '#B31220',
    alignItems: 'center',
    justifyContent: 'center',
    width: '48%',
  },
  buyButton: {
    padding: 10,
    borderWidth: 1,
    borderColor: '#B31220',
    alignItems: 'center',
    justifyContent: 'center',
    width: '48%',
    backgroundColor: '#B31220',
  },
});
