import React from 'react';
import {TabView, SceneMap} from 'react-native-tab-view';
const {height, width} = Dimensions.get('window');
const card_width = width - 40;
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  StatusBar,
  Dimensions,
  Image,
} from 'react-native';

export default function Pending() {
  return (
    <SafeAreaView>
      <StatusBar backgroundColor="white" barStyle="dark-content" />
      <ScrollView>
        <View style={styles.container}>
          <View style={styles.mainContainer}>
            <Text style={styles.LeftText}>Door Lock Issue</Text>
            <Text style={styles.RightText}>Fathima Flat</Text>
          </View>
          <View style={styles.mainContainer}>
            <Text style={styles.LeftText}>Door Lock Issue</Text>
            <Text style={styles.RightText}>Fathima Flat</Text>
          </View>
        </View>
      </ScrollView>
    </SafeAreaView>
  );
}

const styles = StyleSheet.create({
  container: {
    paddingHorizontal: 20,
    paddingTop: 40,
    backgroundColor: '#fff',
  },
  mainContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    backgroundColor: '#F6F6F6',
    paddingHorizontal: 10,
    paddingVertical: '8%',
    borderRadius: 10,
    marginBottom: 16,
  },
  LeftText: {
    fontSize: 16,
  },
  RightText: {
    fontSize: 13,
    color: '#49538C',
  },
});
