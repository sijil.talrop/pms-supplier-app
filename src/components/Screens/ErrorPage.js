import React from 'react';
import {
  SafeAreaView,
  StyleSheet,
  Text,
  View,
  ScrollView,
  TouchableOpacity,
  Dimensions,
  Image,
} from 'react-native';
// import Icon from 'react-native-vector-icons/SimpleLineIcons';

// Icon.loadFont();
const {width, height} = Dimensions.get('window');

const ErrorMessages = () => {
  return (
    <SafeAreaView>
      <View style={styles.mainContainer}>
        <View style={styles.topContainer}>
          <View style={styles.img_container}>
            <Image
              source={require('../../assets/images/vectors/group.png')}
              style={styles.image}
            />
          </View>

          <Text style={styles.title}>Uh oh.</Text>
          <Text style={styles.message}>
            something weird happened keep calm and try again
          </Text>
        </View>

        <TouchableOpacity style={styles.bottomContainer}>
          <Text style={styles.buttonText}>Try again</Text>
        </TouchableOpacity>
      </View>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  mainContainer: {
    // height: height,
    justifyContent: 'space-between',
    padding: 20,
  },
  topContainer: {
    height: height * 0.8,
    alignItems: 'center',
    justifyContent: 'center',
    paddingHorizontal: 30,
  },
  img_container: {
    width: 250,
    height: 200,
  },
  image: {
    width: null,
    flex: 1,
    height: null,
  },
  title: {
    color: '#3F51B5',
    fontWeight: 'bold',
    fontSize: 35,
    marginTop: 40,
    marginBottom: 20,
  },
  message: {
    color: '#757575',
    fontSize: 18,
    textAlign: 'center',
    letterSpacing: 1,
  },
  bottomContainer: {
    // alignItems: 'center',
    paddingVertical: 15,
    backgroundColor: '#3F51B5',
    // borderWidth: 1,
    borderRadius: 15,
  },

  buttonText: {
    color: '#fff',
    textAlign: 'center',
  },
});

export default ErrorMessages;
