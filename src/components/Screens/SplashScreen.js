import React, {useEffect} from 'react';
const {height, width} = Dimensions.get('window');

import {Colors} from 'react-native/Libraries/NewAppScreen';
import {Fonts} from '../../assets/fonts/Fonts';
import * as base from '../../../Settings';
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  StatusBar,
  Dimensions,
  Image,
} from 'react-native';

export default function SplashScreen(props) {
  // const {navigation} = props;
  // useEffect(() => {
  //   setTimeout(() => {
  //     navigation.navigate('OwnedUnit');
  //   }, 1500);
  // });
  return (
    <SafeAreaView>
      <StatusBar backgroundColor="white" barStyle="dark-content" />
      <View>
        <View style={styles.container}>
          <View style={styles.ImageConatiner}>
            <Image
              style={styles.logoImage}
              source={require('../../assets/images/logo.png')}
            />
          </View>

          <Image
            style={styles.bgmImg}
            source={require('../../assets/images/vectors/builing_splash_bg.png')}
          />
        </View>
      </View>
    </SafeAreaView>
  );
}

const styles = StyleSheet.create({
  container: {
    justifyContent: 'center',
    alignItems: 'center',
    minHeight: '100%',
  },
  logoImage: {
    width: null,
    height: null,
    flex: 1,
    resizeMode: 'contain',
  },
  logoText: {
    color: '#000',
    marginTop: '10%',
    fontSize: 18,
  },
  bgmImg: {
    width: width,
    height: width * 0.6,
    left: 0,
    bottom: 0,
    resizeMode: 'contain',
    position: 'absolute',
    width: width,
    opacity: 0.3,
  },
  ImageConatiner: {
    marginBottom: '35%',
    width: width * 0.33,
    height: width * 0.33,
  },
});
