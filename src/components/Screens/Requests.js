import * as React from 'react';
import {
  View,
  ImageBackground,
  StyleSheet,
  Dimensions,
  Text,
} from 'react-native';
import {TabView, SceneMap, TabBar} from 'react-native-tab-view';
import Pending from '../Includes/Pending';
import Processing from '../Includes/Processing';
import Finished from '../Includes/Finished';
import {ScrollView} from 'react-native-gesture-handler';

const {height, width} = Dimensions.get('window');

const FirstRoute = () => (
  <View style={[styles.scene, {backgroundColor: '#ff4081'}]}>
    <Pending />
  </View>
);

const SecondRoute = () => (
  <View style={[styles.scene, {backgroundColor: '#ff4081'}]}>
    <Processing />
  </View>
);

const ThirdRoute = () => (
  <View style={[styles.scene, {backgroundColor: '#ff4081'}]}>
    <Finished />
  </View>
);

const initialLayout = {width: Dimensions.get('window').width};

export default function Requests() {
  const [index, setIndex] = React.useState(0);
  const [routes] = React.useState([
    {key: 'first', title: 'Pending'},
    {key: 'second', title: 'Processing'},
    {key: 'third', title: 'Finished'},
  ]);

  const renderScene = SceneMap({
    first: FirstRoute,
    second: SecondRoute,
    third: ThirdRoute,
  });

  return (
    <View style={{height: height}}>
      <ImageBackground
        source={require('../../assets/images/wallpapers/background.png')}
        style={styles.image}></ImageBackground>
      <Text style={styles.text}>Requests</Text>
      <ScrollView style={{marginTop: -25, backgroundColor: '#fff'}}>
        <View style={styles.MainContainer}>
          <TabView
            navigationState={{index, routes}}
            renderScene={renderScene}
            onIndexChange={setIndex}
            initialLayout={initialLayout}
            renderTabBar={(props) => (
              <TabBar
                {...props}
                indicatorStyle={{
                  backgroundColor: '#3E4FB4',
                }}
                labelStyle={{
                  color: '#000',
                  fontWeight: 'bold',
                  textTransform: 'capitalize',
                }}
                style={{
                  backgroundColor: '#fff',
                  borderTopLeftRadius: 25,
                  borderTopRightRadius: 25,
                }}
              />
            )}
            labelStyle={{
              fontWeight: 'bold',
            }}
            // onIndexChange={(index) => (renderScene.index = index)}
            // initialLayout={{
            //   width: Dimensions.get('window').width,
            // }}
          />
        </View>
      </ScrollView>
    </View>
  );
}

const styles = StyleSheet.create({
  scene: {
    flex: 1,
  },
  image: {
    width: '100%',
    height: 140,
    borderColor: 'red',
    opacity: 0.1,
  },
  text: {
    fontSize: 25,
    color: '#3E4FB4',
    position: 'absolute',
    top: 50,
    left: 25,
    fontWeight: 'bold',
  },
  MainContainer: {
    backgroundColor: '#fff',
  },
});
