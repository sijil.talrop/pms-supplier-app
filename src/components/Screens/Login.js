import React, {useState} from 'react';
import {
  SafeAreaView,
  StyleSheet,
  Text,
  View,
  Image,
  TextInput,
  TouchableOpacity,
  Button,
  Dimensions,
} from 'react-native';
// import { TextField } from "react-native-material-textfield";
// import { Button } from 'react-native-elements';
const {width, height} = Dimensions.get('window');

const Login = () => {
  const [username, setUsername] = useState('username');
  const [password, setPassword] = useState('password');

  return (
    <SafeAreaView>
      <View style={styles.container}>
        <View style={[styles.middleContainer]}>
          <View style={styles.imageContainer}>
            <Image
              source={require('../../assets/images/logo.png')}
              style={styles.image}
            />
          </View>
          <Text style={styles.text}>Login to Continue</Text>
          <View style={styles.loginUser}>
            <TextInput
              style={styles.input}
              placeholder="Username"
              onChangeText={(val) => setUsername(val)}
            />
          </View>
          <View style={styles.loginUser}>
            <TextInput
              style={styles.input}
              placeholder="Password"
              onChangeText={(val) => setPassword(val)}
            />
          </View>
          <TouchableOpacity style={styles.bottomContainer}>
            <Text style={styles.buttonText}>Login</Text>
          </TouchableOpacity>
          <View style={styles.BottomTexts}>
            <Text style={styles.BottomText}>Forgot Password?</Text>
          </View>
        </View>
        <Image
          source={require('../../assets/images/vectors/builing_splash_bg.png')}
          style={styles.imageBottom}
        />
      </View>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  container: {
    minHeight: '100%',
  },

  box: {},
  imageContainer: {
    width: width * 0.25,
    height: width * 0.25,
  },
  text: {
    marginVertical: 20,
    fontSize: 16,
    fontFamily: 'ProductSans-Regular',
  },
  image: {
    width: null,
    height: null,
    flex: 1,
    resizeMode: 'contain',
  },
  middleContainer: {
    paddingHorizontal: 40,
    alignItems: 'center',
    justifyContent: 'center',
    height: height * 0.7,
  },
  input: {
    borderBottomWidth: 1,
    width: width - 80,
    borderColor: '#ccc',
    fontFamily: 'ProductSans-Regular',
  },
  bottomContainer: {
    width: '100%',
    paddingVertical: 15,
    backgroundColor: '#3F51B5',
    borderRadius: 10,
    marginVertical: 25,
  },
  main: {
    borderWidth: 2,
    borderColor: 'red',
    paddingHorizontal: 20,
  },

  buttonText: {
    color: '#fff',
    textAlign: 'center',
    fontFamily: 'ProductSans-Regular',
  },
  BottomText: {
    textAlign: 'center',
  },
  BottomImageContainer: {
    width: width,
    height: width * 0.8, // height: 300,
  },
  // imageContaner: {
  //   position: 'absolute',
  //   bottom: 0,
  //   borderWidth: 2,
  //   left: 0,
  //   width: width * 0.33,
  //   height: width * 0.33,
  // },
  imageBottom: {
    width: width,
    height: width * 0.6,
    left: 0,
    bottom: 0,
    resizeMode: 'contain',
    position: 'absolute',
    width: width,
    opacity: 0.3,
  },
});

export default Login;
