import React, {useState} from 'react';
import {
  SafeAreaView,
  StyleSheet,
  Text,
  View,
  Image,
  TextInput,
  TouchableOpacity,
  Button,
  Dimensions,
  ScrollView,
} from 'react-native';
const {width, height} = Dimensions.get('window');

const ServiceRequest = () => {
  const jobs = [
    {
      text: 'Accepted Job 21',
    },
    {
      text: 'Rejected Job',
    },
    {
      text: 'Pending Request',
    },
    {
      text: 'Completed Job',
    },
  ];

  const Request = [
    {
      text: 'Window Brocken',
      flat: 'Senhor Flat',
    },
    {
      text: 'Rejected Job',
      flat: 'Sky line',
    },
  ];
  return (
    <ScrollView contentContainerStyle={styles.mainContentContainer}>
      <View style={styles.topBox}>
        <View>
          <Text style={styles.name}>Warner Dio</Text>
          <Text style={styles.job}>Supplier</Text>
        </View>
        <View style={styles.image_container}>
          <Image
            source={require('../../assets/images/wallpapers/profile.jpg')}
            style={styles.image}
          />
        </View>
      </View>
      <View style={styles.middleBox}>
        {jobs.map((item) => (
          <TouchableOpacity
            key={item.key}
            style={{
              marginBottom: 20,
            }}>
            <View style={styles.boxes}>
              <View style={styles.imageContainer}>
                <Image
                  source={require('../../assets/images/icons/file.png')}
                  style={styles.icon}
                />
              </View>
              <Text style={styles.contentText}>{item.text}</Text>
            </View>
          </TouchableOpacity>
        ))}
      </View>
      <View>
        <Text style={styles.header}>Service Request</Text>
        {Request.map((item, index) => (
          <View style={styles.service}>
            <Text style={styles.requestHead}>{item.text}</Text>
            <Text style={styles.description}>{item.flat}</Text>
          </View>
        ))}
      </View>
    </ScrollView>
  );
};

const styles = StyleSheet.create({
  mainContentContainer: {
    paddingHorizontal: 20,
    paddingVertical: '13%',
    minHeight: '100%',
  },
  name: {
    fontSize: 25,
    fontWeight: 'bold',
    color: '#3f51b5',
  },
  job: {
    fontSize: 15,
    color: '#aaa',
  },
  topBox: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    marginBottom: '7%',
  },
  image_container: {
    width: 100,
    height: 100,
    borderRadius: 50,
    overflow: 'hidden',
    elevation: 2,
  },
  image: {
    width: '100%',
    height: '100%',
  },
  boxes: {
    flexDirection: 'row',
    width: width * 0.43,
    borderRadius: 15,
    borderWidth: 0.5,
    borderColor: '#ccc',
    paddingHorizontal: 30,
    paddingVertical: 20,
    alignItems: 'center',
    elevation: 3,
    backgroundColor: '#fff',
  },
  imageContainer: {
    width: 40,
    height: 40,
    marginRight: 5,
  },
  icon: {
    flex: 1,
    width: null,
    height: null,
    resizeMode: 'contain',
  },
  middleBox: {
    flexDirection: 'row',
    flexWrap: 'wrap',
    justifyContent: 'space-around',
  },
  contentText: {
    marginTop: 5,
    color: '#3F51B5',
    fontSize: 10,
    textAlign: 'left',
  },
  service: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginBottom: '10%',
  },
  header: {
    fontSize: 21,
    marginBottom: '7%',
    color: '#696868',
  },
  requestHead: {
    fontSize: 17,
    color: '#696868',
  },
  description: {
    fontSize: 14,
    color: '#3f51b5',
  },
});

export default ServiceRequest;
