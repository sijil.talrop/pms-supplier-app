import React from 'react';
const {height, width} = Dimensions.get('window');
const card_width = width - 40;
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  StatusBar,
  Dimensions,
  Image,
} from 'react-native';

export default function Notifications() {
  return (
    <SafeAreaView>
      <StatusBar backgroundColor="white" barStyle="dark-content" />
      <ScrollView>
        <View style={styles.container}>
          <Text style={styles.heading}>Notifications</Text>
          <View style={styles.BoxShadow}>
            <View style={styles.mainBox}>
              <View style={styles.LeftBox}>
                <View style={styles.ImageBox}>
                  <Image
                    source={require('../../assets/images/icons/feather.png')}
                    style={styles.image}
                  />
                </View>
                <View style={styles.ContentBox}>
                  <Text>Service Charge</Text>
                  <Text style={styles.DescriptionText}>
                    Quisque suscipit ipsum est,venenatis leo ornare eget.Up
                    porta facilisis lelementum.sed
                  </Text>
                </View>
              </View>

              <View style={styles.DateBox}>
                <Text style={styles.DescriptionText}>25 jun</Text>
              </View>
            </View>
          </View>

          <View style={styles.BoxShadow}>
            <View style={styles.mainBox}>
              <View style={styles.LeftBox}>
                <View style={styles.ImageBox}>
                  <Image
                    source={require('../../assets/images/icons/checks.png')}
                    style={styles.image}
                  />
                </View>
                <View style={styles.ContentBox}>
                  <Text>Service Charge</Text>
                  <Text style={styles.DescriptionText}>
                    Quisque suscipit ipsum est,venenatis leo ornare eget.Up
                    porta facilisis lelementum.sed
                  </Text>
                </View>
              </View>

              <View style={styles.DateBox}>
                <Text style={styles.DescriptionText}>25 jun</Text>
              </View>
            </View>
          </View>
          <View style={styles.BoxShadow}>
            <View style={styles.mainBox}>
              <View style={styles.LeftBox}>
                <View style={styles.ImageBox}>
                  <Image
                    source={require('../../assets/images/icons/clocks.png')}
                    style={styles.image}
                  />
                </View>
                <View style={styles.ContentBox}>
                  <Text>Service Charge</Text>
                  <Text style={styles.DescriptionText}>
                    Quisque suscipit ipsum est,venenatis leo ornare eget.Up
                    porta facilisis lelementum.sed
                  </Text>
                </View>
              </View>

              <View style={styles.DateBox}>
                <Text style={styles.DescriptionText}>25 jun</Text>
              </View>
            </View>
          </View>
          <View style={styles.BoxShadow}>
            <View style={[styles.mainBox, {backgroundColor: '#f2f2f2'}]}>
              <View style={styles.LeftBox}>
                <View style={styles.ImageBox}>
                  <Image
                    source={require('../../assets/images/icons/clock.png')}
                    style={styles.image}
                  />
                </View>
                <View style={styles.ContentBox}>
                  <Text>Service Charge</Text>
                  <Text style={styles.DescriptionText}>
                    Quisque suscipit ipsum est,venenatis leo ornare eget.Up
                    porta facilisis lelementum.sed
                  </Text>
                </View>
              </View>

              <View style={styles.DateBox}>
                <Text style={[styles.DescriptionText, {color: '#aaa'}]}>
                  25 jun
                </Text>
              </View>
            </View>
          </View>

          <View style={styles.BoxShadow}>
            <View style={[styles.mainBox, {backgroundColor: '#f2f2f2'}]}>
              <View style={styles.LeftBox}>
                <View style={styles.ImageBox}>
                  <Image
                    source={require('../../assets/images/icons/check.png')}
                    style={styles.image}
                  />
                </View>
                <View style={styles.ContentBox}>
                  <Text>Service Charge</Text>
                  <Text style={styles.DescriptionText}>
                    Quisque suscipit ipsum est,venenatis leo ornare eget.Up
                    porta facilisis lelementum.sed
                  </Text>
                </View>
              </View>

              <View style={styles.DateBox}>
                <Text style={[styles.DescriptionText, {color: '#aaa'}]}>
                  25 jun
                </Text>
              </View>
            </View>
          </View>

          <View style={styles.BoxShadow}>
            <View style={styles.mainBox}>
              <View style={styles.LeftBox}>
                <View style={styles.ImageBox}>
                  <Image
                    source={require('../../assets/images/icons/feather.png')}
                    style={styles.image}
                  />
                </View>
                <View style={styles.ContentBox}>
                  <Text>Service Charge</Text>
                  <Text style={styles.DescriptionText}>
                    Quisque suscipit ipsum est,venenatis leo ornare eget.Up
                    porta facilisis lelementum.sed
                  </Text>
                </View>
              </View>

              <View style={styles.DateBox}>
                <Text style={styles.DescriptionText}>25 jun</Text>
              </View>
            </View>
          </View>
        </View>
      </ScrollView>
    </SafeAreaView>
  );
}

const styles = StyleSheet.create({
  container: {
    paddingHorizontal: 20,
    paddingTop: 40,
    backgroundColor: '#fff',
    height: height,
  },
  heading: {
    fontSize: 28,
    fontWeight: 'bold',
    color: '#787878',
    marginBottom: 20,
  },

  mainBox: {
    flexDirection: 'row',
    justifyContent: 'space-between', // alignItems: 'center',
    borderColor: '#fff',
    width: card_width,
    padding: 10,
    borderRadius: 10,
    // elevation: 1,
    overflow: 'hidden',
    borderWidth: 0.5,
    borderColor: '#C2C2C2',
  },
  ImageBox: {
    width: 30,
    height: 30,
    marginRight: 10,
  },
  image: {
    width: '100%',
    height: '100%',
  },
  ContentBox: {},
  DateBox: {
    width: card_width * 0.2,
    flexDirection: 'row',
    justifyContent: 'flex-end',
  },
  LeftBox: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    width: card_width * 0.7,
  },
  DescriptionText: {
    fontSize: 12,
  },
  BoxShadow: {
    // elevation: 5,
    // backgroundColor:"#f2f2f2"
    marginBottom: 20,
  },
});
