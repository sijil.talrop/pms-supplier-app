import React from 'react';
import {
  SafeAreaView,
  StyleSheet,
  Text,
  View,
  ScrollView,
  TouchableOpacity,
  Dimensions,
  Image,
} from 'react-native';
// import Icon from 'react-native-vector-icons/SimpleLineIcons';

// Icon.loadFont();
const {width, height} = Dimensions.get('window');

const Success = () => {
  return (
    <SafeAreaView>
      <View style={styles.mainContainer}>
        <View style={styles.topContainer}>
          <View style={styles.img_container}>
            <Image
              source={require('../../assets/images/vectors/success.png')}
              style={styles.image}
            />
          </View>

          <Text style={styles.title}>Successful</Text>
          <Text style={styles.message}>
            Your service request has been successfuly uploaded
          </Text>
        </View>

        <TouchableOpacity style={styles.bottomContainer}>
          <Text style={styles.buttonText}>I am done!</Text>
        </TouchableOpacity>
      </View>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  mainContainer: {
    justifyContent: 'space-between',
    padding: 20,
  },
  topContainer: {
    alignItems: 'center',
    height: height * 0.75,
    alignItems: 'center',
    justifyContent: 'center',
    paddingHorizontal: 28,
  },
  img_container: {
    width: 220,
    height: 150,
  },
  image: {
    width: '100%',
    height: '100%',
  },
  title: {
    color: '#3F51B5',
    fontSize: 35,
    marginTop: 40,
    marginBottom: 20,
    fontWeight: 'bold',
  },
  message: {
    color: '#757575',
    fontSize: 18,
    textAlign: 'center',
    letterSpacing: 1,
  },
  bottomContainer: {
    width: '100%',
    paddingVertical: 15,
    backgroundColor: '#3F51B5',
    borderRadius: 15,
  },

  buttonText: {
    color: '#fff',
    textAlign: 'center',
  },
});

export default Success;
