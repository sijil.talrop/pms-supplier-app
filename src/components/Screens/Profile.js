import React from 'react';
import {
  SafeAreaView,
  StyleSheet,
  Text,
  View,
  ScrollView,
  TouchableOpacity,
  Image,
} from 'react-native';
// import Icon from 'react-native-vector-icons/MaterialCommunityIcons';

const ProfilePage = (props) => {
  const {navigation} = props;

  return (
    <SafeAreaView>
      <View style={styles.mainContainer}>
        <View style={styles.container}>
          <View style={styles.ProfileContainer}>
            <View style={styles.iconcontainer}>
              <TouchableOpacity>
                <View style={styles.IconBox}>
                  <Image
                    source={require('../../assets/images/icons/return.png')}
                    style={styles.image}
                  />
                </View>
              </TouchableOpacity>
              <TouchableOpacity>
                <View style={styles.IconBox}>
                  <Image
                    source={require('../../assets/images/icons/notification.png')}
                    style={styles.image}
                  />
                </View>
              </TouchableOpacity>
            </View>
            <View style={styles.header}>
              <Text style={styles.name}>Pofile</Text>
            </View>
            <View style={styles.ImgBox}>
              <View style={styles.image_container}>
                <Image
                  source={require('../../assets/images/wallpapers/profile.jpg')}
                  style={styles.image}
                />
              </View>
            </View>
          </View>
          <View style={styles.NameBox}>
            <TouchableOpacity
              onPress={() => navigation.navigate('Success')}
              style={styles.editIcon}>
              <Image
                source={require('../../assets/images/icons/editing.png')}
                style={styles.image}
              />
            </TouchableOpacity>
            <Text style={styles.NameText}>Martin</Text>
          </View>
          <View style={styles.middle}>
            <View style={styles.adrress_container}>
              <Text style={styles.description}>New Orleans</Text>
              <Text style={styles.lightColorText}>Address</Text>
            </View>
            <View style={styles.adrress_container}>
              <Text style={styles.description}>+91063266598</Text>
              <Text style={styles.lightColorText}>Phone Number</Text>
            </View>
            <View style={styles.adrress_container}>
              <Text style={styles.description}>sample@gmail.com</Text>
              <Text style={styles.lightColorText}>Email ID</Text>
            </View>
            <View style={styles.adrress_container}>
              <Text style={styles.description}>
                Kerala 632659,india(Malappuram)
              </Text>
              <Text style={styles.lightColorText}>Location</Text>
            </View>
            <View style={styles.last_view}>
              <Text style={styles.description}>Male</Text>
              <Text style={styles.lightColorText}>Gender</Text>
            </View>
          </View>
          <TouchableOpacity style={styles.button}>
            <View style={styles.buttonBox}>
              <Image
                source={require('../../assets/images/icons/logout.png')}
                style={styles.image}
              />
            </View>
            <Text style={styles.LogoutText}>Log out</Text>
          </TouchableOpacity>
        </View>
      </View>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  mainContainer: {
    minHeight: '100%',
    backgroundColor: '#FFF',
  },
  iconcontainer: {
    justifyContent: 'space-between',
    flexDirection: 'row',
    alignItems: 'center',
    marginBottom: 10,
  },
  IconBox: {
    width: 25,
    height: 25,
  },
  container: {
    backgroundColor: '#FFF',
  },

  ProfileContainer: {
    backgroundColor: '#4F5A9C',
    paddingHorizontal: 20,
    paddingVertical: 30,
  },
  editIcon: {
    width: 35,
    height: 35,
    position: 'absolute',
    right: 20,
    bottom: 29,
    backgroundColor: '#fff',
    borderRadius: 50,
    padding: 7,
    elevation: 4,
  },
  header: {
    marginTop: 10,
  },

  image_container: {
    width: 100,
    height: 100,
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: 50,
    overflow: 'hidden',
    elevation: 2,
  },
  image: {
    width: '100%',
    height: '100%',
  },
  ImgBox: {
    width: '100%',
    alignItems: 'center',
  },
  NameBox: {
    width: '100%',
    alignItems: 'center',
    paddingVertical: 10,
    backgroundColor: '#fff',
  },
  NameText: {
    fontSize: 20,
    color: '#171dc4',
    fontWeight: 'bold',
  },

  text_container: {
    flexDirection: 'column',
    justifyContent: 'space-around',
  },
  name: {
    fontSize: 25,
    color: '#fff',
    fontFamily: 'ProductSans-Regular',
  },

  adrress_container: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    borderBottomWidth: 1,
    borderColor: '#E1DFE2',
    paddingVertical: 14,
  },
  middle: {
    paddingHorizontal: 20,
    marginBottom: 17,
    backgroundColor: '#fff',
  },
  lightColorText: {
    color: '#B2B2B2',
    fontSize: 14,
    fontFamily: 'ProductSans-Regular',
  },
  description: {
    fontFamily: 'ProductSans-Regular',
    fontSize: 15,
    color: '#000',
  },
  last_view: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingVertical: 14,
  },
  buttonBox: {
    width: 25,
    height: 25,
    marginRight: 10,
  },
  buttonContainer: {
    justifyContent: 'center',
  },
  button: {
    flexDirection: 'row',
    paddingHorizontal: 20,
    paddingVertical: 8,
    alignItems: 'center',
    backgroundColor: '#F2F2F2',
  },
  LogoutText: {
    color: 'red',
  },
});

export default ProfilePage;
