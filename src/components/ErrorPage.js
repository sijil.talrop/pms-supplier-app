import React from 'react';
import {
  SafeAreaView,
  StyleSheet,
  Text,
  View,
  ScrollView,
  TouchableOpacity,
  Dimensions,
  Image,
} from 'react-native';
// import Icon from 'react-native-vector-icons/SimpleLineIcons';

// Icon.loadFont();
const {width, height} = Dimensions.get('window');

const ErrorPage = () => {
  return (
    <SafeAreaView>
      <ScrollView contentContainerStyle={styles.mainContainer}>
        <View style={styles.topContainer}>
          <View style={styles.img_container}>
            <Image
              // source={require('../../assets/images/vectors/group.png')}
              style={styles.image}
            />
          </View>

          <Text style={styles.title}>Uh oh.</Text>
          <Text style={styles.message}>
            something weird happened keep calm and try again
          </Text>
        </View>

        <TouchableOpacity style={styles.bottomContainer}>
          <Text style={styles.buttonText}>Try again</Text>
        </TouchableOpacity>
        <View style={styles.topContainer}>
          <View style={styles.img_container}>
            <Image
              // source={require('../../assets/images/vectors/group.png')}
              style={styles.image}
            />
          </View>

          <Text style={styles.title}>Uh oh.</Text>
          <Text style={styles.message}>
            something weird happened keep calm and try again
          </Text>
        </View>

        <TouchableOpacity style={styles.bottomContainer}>
          <Text style={styles.buttonText}>Try again</Text>
        </TouchableOpacity>
      </ScrollView>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  mainContainer: {
    // height: height,
    justifyContent: 'space-between',
    padding: 20,
  },
  topContainer: {
    alignItems: 'center',
    height: height - 99,
    alignItems: 'center',
    justifyContent: 'center',
    paddingHorizontal: 28,
  },
  img_container: {
    // backgroundColor: "red",
    width: 250,
    height: 200,
  },
  image: {
    width: '100%',
    height: '100%',
  },
  title: {
    color: '#3F51B5',
    fontSize: 35,
    marginTop: 40,
    marginBottom: 20,
    fontFamily: 'ProductSans-Regular',
  },
  message: {
    color: '#757575',
    fontSize: 18,
    fontFamily: 'ProductSans-Regular',
    textAlign: 'center',
    letterSpacing: 1,
  },
  bottomContainer: {
    // alignItems: 'center',
    width: '100%',
    paddingVertical: 15,
    backgroundColor: '#3F51B5',
    // borderWidth: 1,
    borderRadius: 15,
  },

  buttonText: {
    color: '#fff',
    textAlign: 'center',
    fontFamily: 'ProductSans-Regular',
  },
});

export default ErrorPage;
