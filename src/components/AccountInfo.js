import React, {useState} from 'react';
const {height, width} = Dimensions.get('window');
import {Colors} from 'react-native/Libraries/NewAppScreen';
import {Fonts} from '../assets/fonts/Fonts';
import * as base from '../../Settings';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
// import ImageOverlay from "react-native-image-overlay";

import {
  StyleSheet,
  ScrollView,
  View,
  TouchableOpacity,
  Image,
  Text,
  Dimensions,
} from 'react-native';
const images = [
  {
    key: 1,
    image: require('../assets/images/icons/bank.png'),
    text: 'Service Charge Demand',
  },
  {
    key: 2,
    image: require('../assets/images/icons/Outline.png'),
    text: 'Ground Rent Demand',
  },
  {
    key: 3,
    image: require('../assets/images/icons/estimate.png'),
    text: 'Account Information',
  },
  {
    key: 4,
    image: require('../assets/images/icons/policy.png'),
    text: 'Service Charge Demand',
  },
];

export default function AccountInfo() {
  const [lineHieght, setLineHieght] = useState(3);
  const [dotWidth, setdotWidth] = useState(20);

  return (
    <View style={styles.container}>
      <View style={styles.AccInfo}>
        <Text
          style={{
            fontFamily: Fonts.ProductSansBold,
            // fontWeight: 'bold',
            color: '#646464',
            fontSize: 16,
            marginBottom: 20,
          }}>
          Account Information
        </Text>

        <ScrollView
          horizontal
          showsHorizontalScrollIndicator={false}
          style={styles.scrollStyle}
          // onScroll={change}
        >
          {images.map((item) => (
            <TouchableOpacity key={item.key} style={{marginRight: 20}}>
              <View style={styles.boxes}>
                <View style={styles.imageContainer}>
                  <Image source={item.image} style={styles.image} />
                </View>
                <Text
                  style={{
                    marginTop: 5,
                    color: '#3F51B5',
                    fontSize: 10,
                    textAlign: 'center',
                  }}>
                  {item.text}
                </Text>
              </View>
            </TouchableOpacity>
          ))}
        </ScrollView>
      </View>
      <View style={styles.request}>
        <View style={styles.head}>
          <Text
            style={{
              fontFamily: Fonts.ProductSansBold,
              // fontWeight: 'bold',
              color: '#646464',
              fontSize: 16,
              marginBottom: 20,
            }}>
            Your Issue Request
          </Text>
          <View style={styles.addIconContainer}>
            <Image
              source={require('../assets/images/icons/add.png')}
              style={styles.addIcon}
            />
          </View>
        </View>
        <View style={styles.cards}>
          <View style={styles.card}>
            <Text style={{fontSize: 14, color: '#686868'}}>Window Broken</Text>
            <Text style={{fontSize: 12, color: '#3F51B5'}}>Sky line</Text>
          </View>
          <View style={styles.card}>
            <Text style={{fontSize: 14, color: '#686868'}}>
              Door Lock Issue
            </Text>
            <Text style={{fontSize: 12, color: '#3F51B5'}}>Fathima Flat</Text>
          </View>
        </View>
      </View>
      <View style={styles.status}>
        <View style={styles.head}>
          <Text
            style={{
              fontFamily: Fonts.OpenSansBold,
              fontWeight: 'bold',
              color: '#646464',
            }}>
            Status
          </Text>
        </View>
        <View style={styles.statusContent}>
          <View
            style={[
              styles.left,
              {justifyContent: 'center', alignItems: 'center'},
            ]}>
            <View style={styles.item}>
              <View style={[styles.Number, {elevation: 3}]}>
                <Text style={{color: '#fff', fontSize: 12}}>1</Text>
              </View>
              <Text style={styles.progressText}>Request</Text>
            </View>
            <View style={[styles.connectLine, {height: lineHieght}]} />
            <View
              style={styles.item}
              onLayout={({nativeEvent}) => {
                setLineHieght(nativeEvent.layout.height);
              }}>
              <View
                style={[
                  styles.Number,
                  {backgroundColor: '#fff', elevation: 3},
                ]}>
                <Text style={{color: '#000', fontSize: 12}}>2</Text>
              </View>
              <Text style={styles.progressText}> Pending</Text>
            </View>
            <View style={styles.item}>
              <View
                style={[
                  styles.Number,
                  {backgroundColor: '#fff', elevation: 3},
                ]}>
                <Text style={{color: '#000', fontSize: 12}}>3</Text>
              </View>
              <Text style={styles.progressText}>3 Accept</Text>
            </View>
          </View>
          <View style={styles.right}>
            <Image
              source={require('../assets/images/icons/status.png')}
              style={styles.statusImage}
            />
          </View>
        </View>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    paddingVertical: 20,
    backgroundColor: '#fff',
  },
  boxes: {
    height: 100,
    width: 90,
    alignItems: 'center',
    justifyContent: 'center',
    marginRight: 2,
    shadowColor: '#000',
    paddingVertical: 5,
    borderRadius: 2,
    // borderColor: '#ccc',
    borderWidth: 0.1,
    // elevation: 0.1,
    overflow: 'hidden',
  },
  imageContainer: {
    overflow: 'hidden',
    height: 40,
    width: 40,
  },
  image: {
    flex: 1,
    width: null,
    height: null,
    resizeMode: 'contain',
  },
  AccInfo: {
    marginBottom: 20,
  },
  head: {
    marginVertical: 20,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'flex-start',
  },
  addIconContainer: {
    padding: 5,
    borderRadius: 5,
    // borderWidth: 0.5,
    borderColor: '#ccc',
    elevation: 1,
    backgroundColor: '#fff',
  },
  addIcon: {
    width: 15,
    height: 15,
  },
  card: {
    backgroundColor: '#F6F6F6',
    flexDirection: 'row',
    justifyContent: 'space-between',
    padding: 15,
    borderRadius: 10,
    alignItems: 'center',
    marginBottom: 10,
  },
  status: {
    // paddingVertical: 40,
  },
  statusContent: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  right: {
    height: 150,
    width: '30%',
  },
  statusImage: {
    flex: 1,
    width: null,
    height: null,
    resizeMode: 'contain',
  },
  progressText: {
    // marginBottom: 20,
    fontFamily: Fonts.OpenSansBold,
    color: '#6B6B6B',
    fontSize: 12,
  },
  item: {
    flexDirection: 'row',
    justifyContent: 'center',
    marginBottom: 30,
    alignItems: 'center',
  },
  Number: {
    width: 24,
    height: 24,
    borderRadius: 12,
    backgroundColor: '#3F51B5',
    justifyContent: 'center',
    alignItems: 'center',
    marginRight: 10,
    textAlign: 'center',
  },
  connectLine: {
    backgroundColor: '#3F51B5',
    borderRadius: 15,
    position: 'absolute',
    top: 22,
    left: 11,
    width: 5,
    elevation: 3,
  },
});
