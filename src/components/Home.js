import React from 'react';
const {height, width} = Dimensions.get('window');
import {Colors} from 'react-native/Libraries/NewAppScreen';
import {Fonts} from '../assets/fonts/Fonts';
import * as base from '../../Settings';
import AccountInfo from './AccountInfo';
// import ImageOverlay from "react-native-image-overlay";

import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  TouchableOpacity,
  Image,
  Text,
  StatusBar,
  Dimensions,
  ImageBackground,
} from 'react-native';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import {useLinkProps} from '@react-navigation/native';

const images = [
  {
    key: 1,
    image: require('../assets/images/wallpapers/flat1.png'),
    title: 'Sky line',
    text: 'Flat No. 1 , St Johns Wood,London NW8 9UJ',
  },
  {
    key: 2,
    image: require('../assets/images/wallpapers/flat2.jpg'),
    title: 'Sky line',
    text: 'Flat No. 1 , St Johns Wood,London NW8 9UJ',
  },
  {
    key: 3,
    image: require('../assets/images/wallpapers/flat3.jpg'),
    title: 'Sky line',
    text: 'Flat No. 1 , St Johns Wood,London NW8 9UJ',
  },
];

export default function OwnedUnitHome(props) {
  const {navigation} = props;

  return (
    <SafeAreaView>
      <StatusBar backgroundColor="white" barStyle="dark-content" />
      <ScrollView>
        <View style={styles.container}>
          <View style={styles.spotlight}>
            <View style={styles.header}>
              <View style={styles.headerLeft}>
                <Text
                  style={{
                    color: '#75B1B2',
                    fontSize: 12,
                    fontFamily: Fonts.ProductSansRegular,
                  }}>
                  Hello
                </Text>
                <Text
                  style={{
                    color: '#3F51B5',
                    // fontWeight: 'bold',
                    fontSize: 23,
                    fontFamily: Fonts.ProductSansBold,
                  }}>
                  Martin
                </Text>
              </View>
              <View style={styles.headerRight}>
                <TouchableOpacity>
                  <Icon name={'bell-outline'} style={styles.bellIcon} />
                </TouchableOpacity>
                <View style={styles.dot} />
              </View>
            </View>
            <View style={styles.OwnedUnits}>
              <Text
                style={{
                  fontFamily: Fonts.ProductSansBold,
                  // fontWeight: 'bold',
                  color: '#646464',
                  fontSize: 16,
                  marginBottom: 20,
                }}>
                Owned Units
              </Text>
              <ScrollView
                horizontal
                showsHorizontalScrollIndicator={false}
                style={styles.scrollStyle}
                // onScroll={change}
              >
                {images.map((item) => (
                  <TouchableOpacity
                    key={item.key}
                    onPress={() =>
                      navigation.navigate('OwnedUnit', {
                        image: item.image,
                      })
                    }
                    style={[styles.ImageBox, {marginRight: 20}]}>
                    <ImageBackground
                      source={item.image}
                      style={{
                        height: 150,
                        width: 260,
                      }}>
                      <View style={styles.innerText}>
                        <Text style={{fontWeight: 'bold', color: '#fff'}}>
                          {item.title}{' '}
                        </Text>
                        <Text style={{color: '#fff', fontSize: 12}}>
                          {item.text}{' '}
                        </Text>
                      </View>
                    </ImageBackground>
                  </TouchableOpacity>
                ))}
              </ScrollView>
            </View>
            <AccountInfo />
          </View>
        </View>
      </ScrollView>
    </SafeAreaView>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    padding: 20,
  },
  spotlight: {
    // background: require('../assets/images/wallpapers/background.png')
  },
  header: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginBottom: 20,
  },
  innerText: {
    fontWeight: 'bold',
    color: 'white',
    position: 'absolute', // child
    bottom: 40, // position where you want
    left: 20,
    fontWeight: 'bold',
    color: 'white',
    width: '70%',
  },
  slideImage: {
    overflow: 'hidden',
    height: 200,
    width: 300,
  },
  image: {
    flex: 1,
    width: null,
    height: null,
    resizeMode: 'contain',
  },
  ImageBox: {
    // borderColor: '#e2e2e2',
    // borderWidth: 0.2,
    borderRadius: 15,
    overflow: 'hidden',
    elevation: 1.9,
  },
  dot: {
    position: 'absolute',
    backgroundColor: '#d32f2f',
    width: 12,
    height: 12,
    borderRadius: 50,
  },
  bellIcon: {
    fontSize: 28,
  },
  OwnedUnits: {
    marginBottom: 20,
  },
});
