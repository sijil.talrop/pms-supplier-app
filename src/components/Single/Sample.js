import React from 'react';
const {height, width} = Dimensions.get('window');

import {Colors} from 'react-native/Libraries/NewAppScreen';
import {Fonts} from '../../assets/fonts/Fonts';
import * as base from '../../../Settings';
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  StatusBar,
  Dimensions,
} from 'react-native';

export default function Sample() {
  return (
    <SafeAreaView>
      <StatusBar backgroundColor="white" barStyle="dark-content" />
      <ScrollView>
        <View style={styles.container}>
          <Text>Request Page</Text>
        </View>
      </ScrollView>
    </SafeAreaView>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    height: height,
    justifyContent: 'center',
    alignItems: 'center',
  },
});
