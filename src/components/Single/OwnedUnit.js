import React, {useState, useEffect} from 'react';
const {height, width} = Dimensions.get('window');
import {Colors} from 'react-native/Libraries/NewAppScreen';
import {Fonts} from '../../assets/fonts/Fonts';
// import ImageOverlay from "react-native-image-overlay";
import Modal from 'react-native-modal';
import ServiceRequests from '../Includes/modals/ServiceRequests';
import ImageUploading from '../Includes/modals/ImageUploading';
import Icon from 'react-native-vector-icons/FontAwesome5';

import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  TouchableOpacity,
  Image,
  Text,
  StatusBar,
  Dimensions,
  ImageBackground,
  // Modal,
} from 'react-native';

const demands = [
  {key: 1, text: 'Service Charge \nRequest'},
  {key: 2, text: 'Ground Rent \nRequest'},
  {key: 3, text: 'Account Information \nRequest'},
  {key: 4, text: 'Service Charge \nRequest'},
];
const cost = [
  {key: 1, text: 'Sched 1', share: '1.36'},
  {key: 2, text: 'Sched 2', share: '1.96'},
  {key: 3, text: 'Sched 3', share: '2.36'},
  {key: 4, text: 'Sched 4', share: '3.36'},
];

export default function OwnedUnit(props) {
  const [isVisible, setVisible] = useState(false);
  const [isUpload, setUpload] = useState(false);
  const {navigation} = props;
  return (
    <SafeAreaView>
      <ScrollView>
        <View style={styles.container}>
          <View style={styles.spotlight}>
            <ImageBackground
              // source={require('../../assets/images/wallpapers/flat1.png')}
              source={props.route.params.image}
              style={{
                height: 300,
                width: width,
                // top: 2,
                // left: 2,
              }}>
              <View style={styles.overlay} />
              <View style={styles.headerLeft}>
                <TouchableOpacity onPress={() => navigation.goBack()}>
                  <Icon name={'arrow-left'} style={styles.backIcon} />
                </TouchableOpacity>
              </View>
              <View style={styles.headerRight}>
                <TouchableOpacity>
                  <Icon name={'bell'} style={styles.bellIcon} />
                </TouchableOpacity>
                <View style={styles.dot} />
              </View>
              <View style={styles.innerText}>
                <Text style={{fontWeight: 'bold', color: '#fff', fontSize: 18}}>
                  Sky Line
                </Text>
                <Text style={{color: '#fff', fontSize: 12}}>
                  Flat No. 1 , St Johns Wood,London NW8 9UJ{' '}
                </Text>
              </View>
            </ImageBackground>
          </View>
          {/* modal_popup */}
          <Modal
            isVisible={isVisible}
            animationOutTiming={1200}
            animationInTiming={1700}
            // animationType={'slide'}
            // transparent={true}
            backdropOpacity={0.1}
            style={{margin: 0}}
            propagateSwipe={true}
            onSwipeComplete={() => setVisible(false)}
            swipeDirection="left"
            onBackdropPress={() => {
              setVisible(false);
            }}
            onBackButtonPress={() => {
              setVisible(false);
            }}>
            <ServiceRequests setVisible={setVisible} setUpload={setUpload} />
          </Modal>
          <Modal
            isVisible={isUpload}
            animationOutTiming={1200}
            animationInTiming={1700}
            // animationType={'slide'}
            // transparent={true}
            backdropOpacity={0.1}
            style={{margin: 0}}
            propagateSwipe={true}
            onSwipeComplete={() => setUpload(false)}
            swipeDirection="left"
            onBackdropPress={() => {
              setUpload(false);
            }}
            onBackButtonPress={() => {
              setUpload(false);
            }}>
            <ImageUploading
              setUpload={setUpload}
              setVisible={setVisible}
              navigation={navigation}
            />
          </Modal>
          <View style={styles.body}>
            <View style={styles.lease}>
              <Text
                style={{
                  fontFamily: Fonts.OpenSansBold,
                  fontWeight: 'bold',
                  color: '#646464',
                  fontSize: 12,
                }}>
                Lease Agreement
              </Text>
              <View style={styles.downloadIconContainer}>
                <Image
                  source={require('../../assets/images/icons/download.png')}
                  style={styles.icon}
                />
              </View>
            </View>
            <View style={styles.service}>
              <Text
                style={{
                  marginBottom: 20,
                  fontFamily: Fonts.OpenSansBold,
                  fontWeight: 'bold',
                  color: '#646464',
                }}>
                Service Charge Demands
              </Text>
              <ScrollView
                horizontal
                showsHorizontalScrollIndicator={false}
                style={styles.scrollStyle}
                // onScroll={change}
              >
                {demands.map((item) => (
                  <TouchableOpacity
                    key={item.key}
                    style={{
                      marginRight: 20,
                      // elevation: 10,
                      // borderWidth: 5,
                      // borderColor: 'red',
                    }}>
                    <View style={styles.boxes}>
                      <View style={styles.imageContainer}>
                        <Image
                          source={require('../../assets/images/icons/file.png')}
                          style={styles.icon}
                        />
                      </View>
                      <Text
                        style={{
                          marginTop: 5,
                          color: '#3F51B5',
                          fontSize: 10,
                          textAlign: 'left',
                        }}>
                        {item.text}
                      </Text>
                    </View>
                  </TouchableOpacity>
                ))}
              </ScrollView>
            </View>
            <View style={styles.request}>
              <View style={styles.head}>
                <Text
                  style={{
                    marginBottom: 20,
                    fontFamily: Fonts.OpenSansBold,
                    fontWeight: 'bold',
                    color: '#646464',
                  }}>
                  Previous Services Requests
                </Text>
              </View>
              <View style={styles.cards}>
                <View style={styles.card}>
                  <Text style={{fontSize: 12, color: '#686868'}}>
                    Window Broken
                  </Text>
                  <Text style={{fontSize: 10, color: '#3F51B5'}}>Sky line</Text>
                </View>
                <View style={styles.card}>
                  <Text style={{fontSize: 12, color: '#686868'}}>
                    Door Lock Issue
                  </Text>
                  <Text style={{fontSize: 10, color: '#3F51B5'}}>
                    Fathima Flat
                  </Text>
                </View>
              </View>
            </View>
            <View style={styles.share}>
              <Text
                style={{
                  marginBottom: 20,
                  fontFamily: Fonts.OpenSansBold,
                  fontWeight: 'bold',
                  color: '#646464',
                }}>
                Share Of Cost
              </Text>
              <ScrollView
                horizontal
                showsHorizontalScrollIndicator={false}
                style={styles.scrollStyle}
                // onScroll={change}
              >
                {cost.map((item) => (
                  <TouchableOpacity key={item.key} style={{marginRight: 20}}>
                    <View style={styles.sharecost}>
                      <Text
                        style={{
                          fontSize: 10,
                          color: '#6B6B6B',
                          textAlign: 'center',
                          fontWeight: 'bold',
                        }}>
                        {item.text}
                      </Text>
                      <Text
                        style={{
                          marginTop: 5,
                          color: '#6B6B6B',
                          fontSize: 9,
                          textAlign: 'center',
                        }}>
                        {item.share}
                      </Text>
                    </View>
                  </TouchableOpacity>
                ))}
              </ScrollView>
            </View>
            <TouchableOpacity
              onPress={() => setVisible(true)}
              activeOpacity={0.6}
              style={styles.button}>
              <Text style={{color: '#fff', fontSize: 14}}>
                New Sevices Request
              </Text>
            </TouchableOpacity>
          </View>
        </View>
      </ScrollView>
    </SafeAreaView>
  );
}

const styles = StyleSheet.create({
  container: {},
  spotlight: {
    // borderWidth: 2,
    borderColor: 'red',
    // background: require('../assets/images/wallpapers/background.png')
  },
  header: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginBottom: 30,
  },
  innerText: {
    fontWeight: 'bold',
    color: 'white',
    position: 'absolute', // child
    bottom: 60, // position where you want
    left: 20,
    fontWeight: 'bold',
    color: 'white',
    width: '70%',
  },
  body: {
    paddingHorizontal: 20,
    paddingVertical: 40,
    marginTop: -40,
    backgroundColor: '#fff',
    // justifyContent: "center",
    alignContent: 'center',
    borderTopLeftRadius: 20,
    borderTopRightRadius: 20,
  },
  lease: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    backgroundColor: '#F6F6F6',
    padding: 20,
    borderRadius: 5,
    marginBottom: 40,
  },
  downloadIconContainer: {
    padding: 5,
    borderRadius: 6,
    width: 20,
    height: 20,
    backgroundColor: '#FFF',
  },
  icon: {
    flex: 1,
    width: null,
    height: null,
    resizeMode: 'contain',
  },
  service: {
    marginBottom: 20,
  },
  boxes: {
    flexDirection: 'row',
    width: 200,
    borderRadius: 15,
    borderWidth: 0.5,
    borderColor: '#ccc',
    paddingHorizontal: 30,
    paddingVertical: 20,
    alignItems: 'center',
  },
  imageContainer: {
    width: 40,
    height: 40,
    marginRight: 5,
  },

  card: {
    backgroundColor: '#F6F6F6',
    flexDirection: 'row',
    justifyContent: 'space-between',
    padding: 15,
    borderRadius: 10,
    alignItems: 'center',
    marginBottom: 10,
  },
  head: {
    marginVertical: 10,
  },
  share: {
    marginTop: 20,
  },
  sharecost: {
    width: 100,
    padding: 20,
    borderWidth: 0.5,
    borderColor: '#ccc',
    borderRadius: 10,
  },
  button: {
    backgroundColor: '#3F51B5',
    padding: 10,
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: 40,
    borderRadius: 10,
  },
  modal: {
    position: 'absolute',
    bottom: 0,
    height: '80%',
    width: '100%',
    padding: 20,
    backgroundColor: '#fff',
  },
  dot: {
    position: 'absolute',
    backgroundColor: '#d32f2f',
    width: 12,
    height: 12,
    borderRadius: 50,
  },
  bellIcon: {
    fontSize: 28,
    color: '#fff',
  },
  headerRight: {
    // margin: '10%'
    position: 'absolute',
    right: 20,
    top: 40,
  },
  backIcon: {
    fontSize: 28,
    color: '#fff',
  },
  headerLeft: {
    position: 'absolute',
    left: 20,
    top: 40,
  },
  overlay: {
    ...StyleSheet.absoluteFillObject,
    backgroundColor: 'rgba(0,0,0,0.2)',
  },
});

// <Modal
//   animationType={'fade'}
//   transparent={true}
//   isVisible={true}
//   animationType={'slide'}
//   transparent={true}
//   backdropOpacity={0.8}
//   swipeDirection={'down'}
//   style={{margin: 0}}
//   onBackdropPress={() => setVisible(false)}
//   propagateSwipe={true}
//   onBackButtonPress={() => {
//     setVisible(false);
//   }}>
//   {/*All views of Modal*/}
//   <View style={styles.modal}>
//     {/* <DeliveryTime onPress={() => {
//                             setVisible({ visible: !visible })
//                         }} /> */}
//     <OrderSummary
//       onPress={() => {
//         setVisible({visible: !visible});
//       }}
//     />
//     {/* <OrderSuccess onPress={() => {
//                             setVisible(false)
//                         }} /> */}
//   </View>
// </Modal>;
